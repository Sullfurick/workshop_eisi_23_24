const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server);

app.use(express.static('public'));

app.get('/public/css/index.css', (req, res) => {
    res.setHeader('Content-Type', 'text/css');
    res.sendFile(__dirname + '/public/css/index.css');
});

app.get('/main', (req, res) => {
    res.sendFile(__dirname + '/html/index.html');
});

app.get('/signalement', (req, res) => {
    res.sendFile(__dirname + '/html/signalement.html');
});

app.get('/reaction', (req, res) => {
    res.sendFile(__dirname + '/html/reaction.html');
});


io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('reaction', (reaction) => {
        console.log('reaction: ' + reaction);
    });
});

io.on('connection', (socket) => {
    socket.on('user message', (msg) => {
        console.log('message: ' + msg);
    });

    socket.on('user message', (msg) => {
        io.emit('user message', msg);
    });
});

io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('reaction', (reaction) => {
        console.log('reaction: ' + reaction);
    });
});

io.on('connection', (socket) => {
    socket.on('signalement_source', (info) => {
        socket.emit('signalement_dest', {lieu: info.lieu, evenement: info.evenement});
        console.log(socket.emit('signalement_dest', {lieu: info.lieu, evenement: info.evenement}))
        console.log(io.emit('signalement_dest', {lieu: info.lieu, evenement: info.evenement}))
    });
});

// Lorsque les utilisateurs réagissent au QR code (peut être déplacé vers le handler sur la page de réaction)
io.on('reaction', (reaction) => {
    // Traitez les réactions et mettez à jour l'affichage en direct
    io.emit('newReaction', reaction);
});

server.listen(3001, () => {
    console.log('listening on *:3001');
});