<h1>WORKSHOP I1 EISI EPSI Rennes 23-24</h1>

<h2>HARASSMENT AT WORK</h2>

******************************************************

**To run the project**
```bash
$ git clone https://gitlab.com/Sullfurick/workshop_eisi_23_24.git
$ cd workshop-i1-eisi-epsi-rennes-23-24
$ npm i
$ node index.js
```

<h3>Project description</h3>
The objective is to imagine a solution that can help the victims of harassment at work.
The solution must be able to help the victim to express himself, to be listened to and to be able to act in order to stop the harassment.
This project in particular aims on denonciation of harassment at work. The victim can anonymously denounce his harasser.

<h3>Technologies used</h3>
<ul>
  <li>JavaScript</li>
  <li>Socket.io</li>
  <li>Bootstrap</li>
</ul>

<h3>Simple schema</h3>
![sexual harassment workshop(1).png](public%2Freadme%2Fsexual%20harassment%20workshop%281%29.png)